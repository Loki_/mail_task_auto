package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;
import pages.HomePage;
import pages.LoginPage;
import pages.MailPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class BaseTest {
    protected WebDriver getDriver() {
        return driver;
    }
    private ChooseUser loadProps;
    private static final String sFileName = "testBrowser.properties";
    private static String sDirSeparator = System.getProperty("file.separator");
    protected static Properties props = new Properties();
    public WebDriver driver;

    public WebDriver chooseBrowsers() throws IOException {
        File currentDir = new File(".");
        String sFilePath = currentDir.getCanonicalPath() + sDirSeparator + sFileName;
        FileInputStream ins = new FileInputStream(sFilePath);
        props.load(ins);
        ins.close();
        //System.out.println(props.getProperty("BROWSER"));
        String browser = props.getProperty("BROWSER");

        switch (browser) {
            case "Chrome":
                return createChromeDriver();
            case "IE":
                return createInternetExplorerDriver();
            default:
                return createFirefoxDriver();
        }
    }
    private WebDriver createChromeDriver() {
        driver = new ChromeDriver();
        return driver;
    }
    private WebDriver createInternetExplorerDriver() {
        driver = new InternetExplorerDriver();
        return driver;
    }
    private WebDriver createFirefoxDriver() {
        driver = new FirefoxDriver();
        return driver;
    }
    @BeforeTest
    public void setUp() throws IOException {
        chooseBrowsers();
        loadProps = ChooseUser.getUsers();
    }
    @BeforeMethod
    public void getPauseAndSize() {
        getDriver().manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        getDriver().manage().window().maximize();
    }
    @AfterSuite
    public void browseClose() {
        driver.close();
        driver.quit();
    }
}
