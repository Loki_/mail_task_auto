package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;

/**
 * Created by anna on 01.11.16.
 */
public class HomePage extends AbstractPage {

    //Login link
    @FindBy (xpath = "//a[@href='/login.jsp']")
    private WebElement loginLink;

    // Method for get URL for login page
    public HomePage openUrl(String url) {
        getDriver().get(url);
        return this;
    }

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public HomePage ckickOnLoginLink(){
        loginLink.click();
        return this;
    }
}
