package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginPage extends AbstractPage {
    // Login field
    @FindBy(xpath="//input[contains(@class, 'input-lg')]")
    private WebElement userName;

    // Password
    @FindBy (xpath = "//input[@id='loginPassword']")
    private WebElement password;

    //Submit
    @FindBy (xpath="//button[contains(@class, 'btn-lg')]")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTextLoginButton(){
        String text = loginButton.getText();
        return text;
    }
    // Method for login
    public LoginPage makeLogin(String login, String pass) {
        userName.clear();
        userName.sendKeys(login);
        //System.out.println(login);
        password.clear();
        password.sendKeys(pass);
        //System.out.println(pass);
        loginButton.click();
        return this;
    }

}
