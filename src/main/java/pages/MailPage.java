package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class MailPage extends AbstractPage {
    //"Inbox" text
    @FindBy (xpath = "//div[contains(@class, 'visible-md')]")
    private WebElement textInbox;

    // [ This Inbox is currently Empty ]
    @FindBy (xpath = "//span[@id='public_count']")
    private WebElement mailCount;

    // Logout
    @FindBy (xpath="//ul/li[7]/a[text()='Logout']")
    private WebElement logout;

    public MailPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String textInbox(){
        String text = textInbox.getText();
        return text;
    }

    public int getMailCount() {
        int mailBox = Integer.parseInt(mailCount.getText());
        return mailBox;
    }

    public MailPage mailLogout(){
        logout.click();
        return this;
    }
}
