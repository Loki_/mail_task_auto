import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

/**
 * Created by anna on 01.11.16.
 */
public class HomeTest extends BaseTest {
    private String URL = "https://www.mailinator.com";
    private String expectedResult = "LOGIN";
    @Test
    private void openSite(){
        HomePage homePage = new HomePage(getDriver());
        homePage.openUrl(URL);
        homePage.ckickOnLoginLink();
        LoginPage loginPage = new LoginPage(getDriver());
        String actualResult = loginPage.getTextLoginButton();
        //System.out.println(actualResult);
        Assert.assertEquals(actualResult, expectedResult, "The site Mailinator was not opens");
    }
}
