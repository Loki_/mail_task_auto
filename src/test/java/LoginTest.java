import base.BaseTest;
import base.ChooseUser;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MailPage;

import java.io.IOException;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginTest extends BaseTest {

    private String expectedresult1 = "Inbox:";
    @Test
    public void loginTest() throws IOException {
        //EntrySet is a Map and its key and value pair is obtained
        ChooseUser.getUsers().userprops.entrySet().stream().forEach(e -> {
            testUser(e.getKey().toString(), e.getValue().toString());
        });
    }
    private void testUser(String username, String password){
        LoginPage loginPage = new LoginPage(getDriver());
        loginPage.makeLogin(username, password);
        MailPage mailPage = new MailPage(getDriver());
        String actualResult = mailPage.textInbox();
        //System.out.println(actualResult);
        Assert.assertEquals(actualResult, expectedresult1, "Login is failed");
    }

}
